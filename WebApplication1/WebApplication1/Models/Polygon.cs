﻿namespace WebApplication1.Models
{
    public class Polygon
    {
        public double CalculateArea(int n, double a)
        {
            double result = n * Math.Pow(a, 2) / (4 * Math.Tan(180 * (Math.PI / 180) / n));
            return result;
        }
    }
}
